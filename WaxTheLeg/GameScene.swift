//
//  GameScene.swift
//  WaxTheLeg
//
//  Created by MacStudent on 2019-06-19.
//  Copyright Â© 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var xd:CGFloat = 0
    var yd:CGFloat = 0
    
    var leg=SKSpriteNode(imageNamed: "leg")
    var hair1=SKSpriteNode(imageNamed: "hair1")
    var hair2=SKSpriteNode(imageNamed: "hair2")
    var hair3=SKSpriteNode(imageNamed: "hair3")
    var hair4=SKSpriteNode(imageNamed: "hair4")
    
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    override func didMove(to view: SKView) {
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    private var currentNode: SKNode?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            
            let touchedNodes = self.nodes(at: location)
           
               let m1 = SKAction.move(
                    to: CGPoint(
                      x:565,
                       y:425),
                   duration: 2)
          
               // move to (0,h/2)
                 let m2 = SKAction.move(
                    to: CGPoint(
                         x:650,
                        y:460),
                     duration: 2)
           
               // move to (w, h/2)
                 let m3 = SKAction.move(           to: CGPoint(
                          x:740,
                          y:465),
                      duration: 2)
            
            
          let m4 = SKAction.removeFromParent();
         
                 let sequence:SKAction = SKAction.sequence([m1, m2, m3,m4 ])
            
            
            

            
            for node in touchedNodes.reversed() {
                if node.name == "hair1" {
                    self.currentNode = node
                    node.run(sequence)
                //    hair1.removeFromParent()
                  //  hair2.removeFromParent()
                //    hair3.removeFromParent()
                 //   hair4.removeFromParent()
                    
                
                }
            }
        }
    }
    override  func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first, let node = self.currentNode {
            let touchLocation = touch.location(in: self)
            node.position = touchLocation
            
            
        }
    }
    
    override   func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if SKNode().name=="hair2"
        {
            self.currentNode = hair2
           
        }
        
        
        
    }
    override  func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.currentNode = hair2
    }
}



